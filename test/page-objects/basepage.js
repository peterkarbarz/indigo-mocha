class BasePage {
	constructor(webdriver, url) {
	  this.webdriver = webdriver;
	  this.url = url;
	}
  }

open = async function (webdriver, url) {
	this.webdriver.get(this.url);
	return this;
};
waitFor = async function(locator, timeout) {
	var waitTimeout = timeout || 20000;
	var webdriver = this.webdriver;
	return webdriver.wait(function() {
		return webdriver.findElements(locator);
	}, waitTimeout);
};

module.exports = BasePage;