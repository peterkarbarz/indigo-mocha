var BasePage = require('../basepage');
var webDriver;
var ItemPage = require('./item-page');


function SearchResultsPage (webdriver) {
	this.driver = webdriver;
	this.driver.get("https://www.indigo.ca/en-ca/home/search/?keywords=");
}

SearchResultsPage.prototype = Object.create(BasePage.prototype);
SearchResultsPage.prototype.constructor = SearchResultsPage;

SearchResultsPage.prototype.getDisplayedResults = function() {
	return this.driver.findElements({ xpath: "//div[@data-a8n='result-count-info']"});
};

SearchResultsPage.prototype.clickFirstAddToCart = function() {
	this.driver.findElement({ xpath: "//div[@class='product-list__results-container']/div[@data-a8n='search-page__product'][1]//button[@data-a8n='search-page__button-add-to-cart']"}).click();
	this.waitFor({ xpath: "//section[@data-a8n='notification__add-to-cart--hide'][@class='notification']"});
	return new SearchResultsPage(this.driver);
};

SearchResultsPage.prototype.clickListView = function() {
	this.driver.findElement({ xpath: "//button[@data-a8n='search-page-listview']"}).click();
	return new SearchResultsPage(this.driver);
};

SearchResultsPage.prototype.verifyFirstElementExistsInSearchResults = function() {
	this.waitFor({ xpath: "//div[@class='product-list__results-container']/div[@data-a8n='search-page__product'][1]"});
}

SearchResultsPage.prototype.clickFirstItemInSearchResults = function() {
	this.driver.findElement({ xpath: "//div[@class='product-list__results-container']/div[@data-a8n='search-page__product'][1]"}).click();
	return new ItemPage(this.driver);
}

module.exports = SearchResultsPage;