var BasePage = require('../basepage');

class ItemPage extends BasePage {

}

ItemPage.prototype.clickAddToCart = function() {
	this.driver.findElement({ xpath: "//button[@id='add-to-cart-button']"}).click();
	return;
};

ItemPage.prototype.getCartItemCount = function() {
	return this.driver.findElement({ xpath: "//div[@data-a8n='common-shopping-cart-count']"});
};

ItemPage.prototype.setQuantity = function(desiredQuantity) {
	this.driver.findElement({ xpath: "//input[@data-a8n='quantity-control__input']"}).clear();
	return this.driver.findElement({ xpath: "//input[@data-a8n='quantity-control__input']"}).sendKeys(desiredQuantity);
};

module.exports = ItemPage;