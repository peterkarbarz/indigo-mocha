var BasePage = require('../basepage');
var SearchResultsPage = require('./search-results-page');

class GlobalHeader extends BasePage {

}

GlobalHeader.prototype.open (webdriver, url) {
	super.open(webdriver, url);
};

GlobalHeader.prototype.typeSearchQuery = function(searchQuery) {
	return this.driver.findElement({ xpath: "//input[@data-a8n='quick-search-input']"}).sendKeys(searchQuery);
};

GlobalHeader.prototype.clickSearchButton = function() {
	this.driver.findElement({ xpath: "//button[@data-a8n='quick-search-submit']"}).click();
	this.waitFor({ xpath: "//div[@class='search__filters-results-container']"});
	return new SearchResultsPage(this.driver);
};

GlobalHeader.prototype.waitForAddToCartNotification = function() {
	this.waitFor({ xpath: "//section[@data-a8n='notification__add-to-cart']"})
	return this.waitFor({ xpath: "//section[@data-a8n='notification__add-to-cart--hide']"});	
}

GlobalHeader.prototype.getCartItemCount = function() {
	return this.driver.findElement({ xpath: "//div[@data-a8n='common-shopping-cart-count']"});
};
module.exports = GlobalHeader;