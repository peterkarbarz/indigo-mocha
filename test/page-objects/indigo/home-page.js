var BasePage = require('../basepage');
var GlobalHeader = require('./global-header');

class HomePage extends BasePage {

	}
	
	HomePage.prototype.getHeader = function () {
		return new GlobalHeader(this.driver);
	};
module.exports = HomePage;