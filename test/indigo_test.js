var	HomePage = require('./page-objects/indigo/home-page');
var globalHeader, homePage, itemPage, cartCounter;
var expect = require('chai').expect;
var assert = require('assert');

describe('Item Page Add to Cart Functionality', () => {
	let driver;

	beforeEach(async ()  => {
		cartCounter = 0;
		driver = require('./driver').getDriver();
		homePage = new HomePage(driver,"www.chapters.indigo.ca");
		globalHeader = await homePage.getHeader();
		await globalHeader.open();
		await globalHeader.typeSearchQuery('big dog');
		var searchResultsPage = await globalHeader.clickSearchButton();
		itemPage = await searchResultsPage.clickFirstItemInSearchResults();
		await itemPage.clickAddToCart();
	});
	afterEach(async () => {
		driver.quit();
	});
	it('cart notification auto-hides', async () => {
		cartCounter ++;
		await globalHeader.waitForAddToCartNotification();
	}).timeout(50000);
	it('check item cart count', async () => {
		cartCounter ++;
		await globalHeader.waitForAddToCartNotification();
		globalHeader.getCartItemCount().getText().then(function(numberOfResults) {
			expect(parseInt(numberOfResults, 10)).to.equal(cartCounter);
		});
	});
	it('add another item to cart and check cart count', async () => {
		cartCounter ++;
		await globalHeader.waitForAddToCartNotification();
		await itemPage.clickAddToCart();
		cartCounter ++;
		await globalHeader.waitForAddToCartNotification();
		globalHeader.getCartItemCount().getText().then(function(numberOfResults) {
			expect(parseInt(numberOfResults, 10)).to.equal(cartCounter);
		});
	});
	/*it('add large quantity to cart and check cart count', async () => {
		cartCounter ++;
		await globalHeader.waitForAddToCartNotification();
		var quantityToAdd = 35;
		await itemPage.setQuantity(quantityToAdd);
		await itemPage.clickAddToCart();
		cartCounter += quantityToAdd;
		await globalHeader.waitForAddToCartNotification();
		globalHeader.getCartItemCount().getText().then(function(numberOfResults) {
			expect(parseInt(numberOfResults, 10)).to.equal(cartCounter);
		});
	});*/
});