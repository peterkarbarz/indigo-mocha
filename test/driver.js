var webdriver = require('selenium-webdriver'),
	driver;

var getDriver = function() {
	//To run tests in the same browser instance uncomment this code and ensure that before / after for tests are updated to not open / close the driver for each test
	// Sets up a new driver every time getDriver is called (this will start a new browser instance for each getDriver call)
	driver = buildDriver();
	return driver;
};

var buildDriver = function() {
	return new webdriver.Builder().
		withCapabilities(webdriver.Capabilities.chrome()).
		build();
};

module.exports.getDriver = getDriver;